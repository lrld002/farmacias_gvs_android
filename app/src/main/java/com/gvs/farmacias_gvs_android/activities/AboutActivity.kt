package com.gvs.farmacias_gvs_android.activities

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.gvs.farmacias_gvs_android.R

class AboutActivity : AppCompatActivity() {

    private lateinit var aboutFacebook: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        findViews()
        setListeners()
    }

    private fun findViews() {
        aboutFacebook = findViewById(R.id.about_button_facebook)
    }

    private fun setListeners() {
        aboutFacebook.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://facebook.com/farmaciagvs"))
            startActivity(intent)
        }
    }

}
