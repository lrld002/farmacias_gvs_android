package com.gvs.farmacias_gvs_android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.gvs.farmacias_gvs_android.R

class ConfigurationActivity : AppCompatActivity() {

    
    private lateinit var configurationLogout: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_configuration)

        findViews()
    }

    private fun findViews() {
        configurationLogout = findViewById(R.id.configuration_logout)
    }

    private fun setListeners() {

    }

}
