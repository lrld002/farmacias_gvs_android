package com.gvs.farmacias_gvs_android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.system.Os.close
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.gvs.farmacias_gvs_android.App
import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.models.Item
import com.gvs.farmacias_gvs_android.models.ShoppingCart
import com.gvs.farmacias_gvs_android.models.User
import kotlinx.android.synthetic.main.activity_item.*

class ItemActivity : BaseActivity() {

    companion object {

        val EXTRA_ITEM_ID = "XTRA_ITEMID"

    }

    private var itemID = ""

    val firebaseUser = FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_item)

        initViews()
        setListeners()

        if (intent.hasExtra(EXTRA_ITEM_ID)) {
            itemID = intent.extras!!.getString(EXTRA_ITEM_ID)!!

            layout_itemName.text = itemID

        }

        App.database_table_items.child(itemID).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val item = snapshot.getValue(Item::class.java)
                if (item != null) {
                    layout_itemName.text = item.name
                    layout_itemPrice.text = item.price.toString()

                    add_to_cart_item.setOnClickListener {
                        App.shoppingCart.items.add(item)
                        db.collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS)
                            .document(firebaseUser!!.uid)
                            .set(App.shoppingCart)
                        Toast.makeText(this@ItemActivity,"Se agregó al carrito", Toast.LENGTH_LONG).show()
                    }
                }
            }

            override fun onCancelled(p0: DatabaseError) {
            }
        })

    }
    override fun initViews() {
    }
    override fun setListeners() {
        val item = Item()
        /*add_to_cart_item.setOnClickListener {
            App.shoppingCart.items.add(item)
            db.collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS)
                .document(firebaseUser!!.uid)
                .set(App.shoppingCart.items.add(item))
            Toast.makeText(this,"Se agregó al carrito", Toast.LENGTH_LONG).show()
        }*/
    }

}
