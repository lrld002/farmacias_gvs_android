package com.gvs.farmacias_gvs_android.activities

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.fragments.AccountFragment
import com.gvs.farmacias_gvs_android.fragments.CartFragment
import com.gvs.farmacias_gvs_android.fragments.HomeFragment
import com.gvs.farmacias_gvs_android.fragments.SearchFragment

class MainActivity : BaseActivity() {

    private lateinit var mainToolbar: androidx.appcompat.widget.Toolbar

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when(item.itemId) {
            R.id.home -> {
                val homeFragment = HomeFragment.newInstance()
                openFragment(homeFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.search -> {
                val searchFragment =  SearchFragment.newInstance()
                openFragment(searchFragment)
                return@OnNavigationItemSelectedListener true
            }
            R.id.account -> {
                val accountFragment = AccountFragment.newInstance()
                openFragment(accountFragment)
                return@OnNavigationItemSelectedListener  true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setListeners()

        openFragment(HomeFragment())

        val bottomNavigation: BottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        mainToolbar = findViewById(R.id.main_toolbar)
        setSupportActionBar(mainToolbar)
    }

    private fun openFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container,fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun initViews() {
    }

    override fun setListeners() {
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {

        menuInflater.inflate(R.menu.toolbar_menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        if (id == R.id.toolbar_cart) {
            Toast.makeText(this, "Action clicked", Toast.LENGTH_LONG).show()
            val cartFragment = CartFragment.newInstance()
            openFragment(cartFragment)
            return true
        }

        return super.onOptionsItemSelected(item)
    }

}
