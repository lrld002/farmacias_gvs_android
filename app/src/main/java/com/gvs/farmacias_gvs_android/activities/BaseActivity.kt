package com.gvs.farmacias_gvs_android.activities

import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.gvs.farmacias_gvs_android.App
import com.gvs.farmacias_gvs_android.models.ShoppingCart
import com.gvs.farmacias_gvs_android.models.User

abstract class BaseActivity: AppCompatActivity() {

    companion object {
        const val TAG = "GVS_LOGIN"
    }

    private lateinit var auth: FirebaseAuth

    val db = FirebaseFirestore.getInstance()

    internal abstract fun initViews()

    internal abstract fun setListeners()

    public override fun onStart() {
        super.onStart()

        auth = FirebaseAuth.getInstance()

        //auth.signOut()

        if (FirebaseAuth.getInstance().currentUser != null) {
            FirebaseFirestore.getInstance().collection(App.FIRESTORE_COLLECTION_USERS)
                .document(auth.currentUser!!.uid)
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        App.user = it.result?.toObject(User::class.java) ?: User()
                        FirebaseFirestore.getInstance().collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS)
                            .whereEqualTo("id", App.user.uid)
                            .limit(1)
                            .get()
                            .addOnCompleteListener {task ->
                                if (task.isSuccessful){
                                    if (task.result != null){
                                        if (!task.result!!.isEmpty){
                                            App.shoppingCart = task.result!!.first().toObject(
                                                ShoppingCart::class.java)
                                        }else{
                                            App.shoppingCart = ShoppingCart(App.user.uid , App.user, ArrayList())
                                        }
                                    }else{
                                        App.shoppingCart = ShoppingCart(App.user.uid , App.user, ArrayList())
                                    }
                                }
                            }
                    }
                }
        } else {
            auth.signInAnonymously().addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInAnonymously:success")
                    val firebaseUser = FirebaseAuth.getInstance().currentUser
                    if (firebaseUser != null) {
                        val user = User(
                            firebaseUser.uid,
                            firebaseUser.email.orEmpty(),
                            "Anon",
                            "",
                            -1,
                            0,
                            false
                        )

                        val cart = ShoppingCart(
                            firebaseUser.uid,
                            user,
                            ArrayList()
                        )

                        db.collection(App.FIRESTORE_COLLECTION_USERS)
                            .document(user.uid)
                            .set(user)

                        db.collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS)
                            .document(user.uid)
                            .set(cart)

                    } else {
                        Log.w(TAG, "signInAnonymously:failure", task.exception)
                        Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                    }
                }

            }
        }

    }
}