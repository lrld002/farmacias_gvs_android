package com.gvs.farmacias_gvs_android.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.RadioGroup
import android.widget.Toast
import com.gvs.farmacias_gvs_android.App
import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.models.Item
import java.util.ArrayList

class EditActivity : AppCompatActivity() {

    private var editItemName: EditText? = null
    private var editItemPrice: EditText? = null
    private var editItemDescription: EditText? = null
    private var editItemFinish: Button? = null
    private val editItemTax: RadioGroup? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit)

        findViews()
        setListeners()
    }

    private fun findViews() {
        editItemName = findViewById<View>(R.id.edit_item_name) as EditText
        editItemPrice = findViewById<View>(R.id.edit_item_price) as EditText
        editItemDescription = findViewById<View>(R.id.edit_item_description) as EditText
        editItemFinish = findViewById<View>(R.id.edit_item_add) as Button

    }

    private fun setListeners() {

        editItemFinish!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val itemID = App.database_table_items.push().key

                val itemName = editItemName!!.text.toString()
                val itemPrice = editItemPrice!!.text.toString()

                if ((!itemName.isBlank() || !itemName.isEmpty() && (!itemPrice.isBlank() || !itemPrice.isEmpty()))) {
                    val item = Item(itemID!!,itemName,false,itemPrice.toDouble(),editItemDescription!!.text.toString(),"",
                        ArrayList<String>(),false,false,""
                    )
                    App.database_table_items.child(itemID).setValue(item)
                    Toast.makeText(this@EditActivity, "Action clicked", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

}
