package com.gvs.farmacias_gvs_android.models

class Item {
    var itemId: String = ""
    var name: String = ""
    var service: Boolean = false
    var price: Double = 0.0
    var text: String = ""
    var department: String = ""
    var category: List<String> = ArrayList()
    var prescription: Boolean = false
    var tax: Boolean = false
    var image: String = ""

    constructor() {

    }

    constructor(
        itemId: String,
        name: String,
        service: Boolean,
        price: Double,
        text: String,
        department: String,
        category: ArrayList<String>,
        prescription: Boolean,
        tax: Boolean,
        image: String
    ) {
        this.itemId = itemId
        this.name = name
        this.service = service
        this.price = price
        this.text = text
        this.department = department
        this.category = category
        this.prescription = prescription
        this.tax = tax
        this.image = image
    }
}