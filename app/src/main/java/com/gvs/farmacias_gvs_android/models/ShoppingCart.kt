package com.gvs.farmacias_gvs_android.models

class ShoppingCart {
    var cartId: String = ""
    lateinit var user: User

    var items: ArrayList<Item> = ArrayList()

    constructor() {

    }

    constructor(cartId: String, user: User, items: ArrayList<Item>) {
        this.cartId = cartId
        this.user = user
        this.items = items
    }


}