package com.gvs.farmacias_gvs_android.models

class User {
    lateinit var uid: String
    lateinit var mail: String
    lateinit var name: String
    lateinit var photoUrl: String
    var gender: Int = 0
    var timeStamp: Long = 0L
    var collaborates: Boolean? = null
    //var text: String? = null

    constructor() {

    }

    constructor(uid: String, mail: String, name: String, photoUrl: String, gender: Int, timeStamp: Long, collaborates: Boolean) {
        this.uid = uid
        this.mail = mail
        this.name = name
        this.photoUrl = photoUrl
        this.gender = gender
        this.timeStamp = timeStamp
        this.collaborates = collaborates
    }
}