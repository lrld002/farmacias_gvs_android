package com.gvs.farmacias_gvs_android.fragments

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.gvs.farmacias_gvs_android.App
import com.gvs.farmacias_gvs_android.App.Companion.FIRESTORE_COLLECTION_SHOPPING_CARTS
import com.gvs.farmacias_gvs_android.App.Companion.FIRESTORE_COLLECTION_USERS
import com.gvs.farmacias_gvs_android.App.Companion.user

import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.activities.AboutActivity
import com.gvs.farmacias_gvs_android.activities.ConfigurationActivity
import com.gvs.farmacias_gvs_android.activities.EditActivity
import com.gvs.farmacias_gvs_android.models.ShoppingCart
import com.gvs.farmacias_gvs_android.models.User

class AccountFragment : Fragment() {

    private var accountId: TextView? = null
    private var editButton: Button? = null

    private var configurationButton: Button? = null
    private var aboutButton: Button? = null

    companion object {
        fun newInstance(): AccountFragment = AccountFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val view: View = inflater.inflate(R.layout.fragment_account,container,false)
        findViews(view)
        setListeners()

        return view
    }

    private fun findViews(v: View) {
        accountId = v.findViewById<View>(R.id.account_id) as TextView
        editButton = v.findViewById<View>(R.id.account_edit_button) as Button
        configurationButton = v.findViewById<View>(R.id.account_button_configuration) as Button
        aboutButton = v.findViewById<View>(R.id.account_button_about) as Button
    }

    private fun setListeners() {

        accountId!!.text = FirebaseAuth.getInstance().currentUser!!.uid

        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null) {
            FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_USERS)
                .document(firebaseUser.uid)
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        user = it.result?.toObject(User::class.java) ?: User()
                        if (user.collaborates == true) {
                            editButton!!.visibility = View.VISIBLE
                        }
                    }
                }

        }
        configurationButton!!.setOnClickListener {
            val intent = Intent(context, ConfigurationActivity::class.java)
            context!!.startActivity(intent)
        }
        aboutButton!!.setOnClickListener {
            val intent = Intent(context, AboutActivity::class.java)
            context!!.startActivity(intent)
        }
        editButton!!.setOnClickListener {
            val intent = Intent(context, EditActivity::class.java)
            context!!.startActivity(intent)
        }
    }

}
