package com.gvs.farmacias_gvs_android.fragments

import android.os.Bundle
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.gvs.farmacias_gvs_android.App

import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.adapters.CartAdapter
import com.gvs.farmacias_gvs_android.models.ShoppingCart
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration

class CartFragment : Fragment() {

    companion object {
        fun newInstance(): CartFragment = CartFragment()
    }

    private lateinit var cartRecyclerView: RecyclerView
    private lateinit var cartAdapter: CartAdapter


    private lateinit var horizontalDivider: HorizontalDividerItemDecoration

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_cart,container,false)
        horizontalDivider = HorizontalDividerItemDecoration.Builder(activity).size(1).margin(
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, resources.displayMetrics).toInt(), 0).build()

        //cartAdapter = CartAdapter(this, this)
        //cartAdapter.list = users


        findViews(view)
        setListeners()

        return view
    }

    private fun findViews(v: View) {
        cartRecyclerView = v.findViewById<View>(R.id.cart_recycler_view) as RecyclerView
        //cartRecyclerView.adapter = cartAdapter
        cartRecyclerView.setHasFixedSize(true)
        cartRecyclerView.layoutManager = LinearLayoutManager(context)
        cartRecyclerView.addItemDecoration(horizontalDivider)

    }

    private fun setListeners() {
        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null) {
            FirebaseFirestore.getInstance().collection(App.FIRESTORE_COLLECTION_SHOPPING_CARTS)
                .document(firebaseUser.uid)
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        /*user = it.result?.toObject(User::class.java) ?: User()
                        if (user.collaborates == true) {
                            editButton!!.visibility = View.VISIBLE
                        }*/
                        App.shoppingCart = it.result?.toObject(ShoppingCart::class.java) ?: ShoppingCart()
                        cartAdapter.notifyDataSetChanged()
                        //updateUI()

                    }
                }

        }
    }

}
