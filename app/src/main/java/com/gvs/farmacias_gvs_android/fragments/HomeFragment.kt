package com.gvs.farmacias_gvs_android.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.GlideContext
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.model.GlideUrl

import com.gvs.farmacias_gvs_android.R

/**
 * A simple [Fragment] subclass.
 */
class HomeFragment : Fragment() {

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }

    private lateinit var advertisement1: ImageView
    private lateinit var advertisement2: ImageView
    private lateinit var advertisement3: ImageView
    private lateinit var advertisement4: ImageView

    private lateinit var fullImage: ImageView
    private lateinit var coordinatorLayout: CoordinatorLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)

        findViews(view)
        setListeners()

        return view

    }

    private fun findViews(v: View) {
        advertisement1 = v.findViewById(R.id.home_advertisement_1) as ImageView
        advertisement2 = v.findViewById(R.id.home_advertisement_2) as ImageView
        advertisement3 = v.findViewById(R.id.home_advertisement_3) as ImageView
        advertisement4 = v.findViewById(R.id.home_advertisement_4) as ImageView
        fullImage = v.findViewById(R.id.home_full_image) as ImageView
        coordinatorLayout = v.findViewById(R.id.home_coordinator_layout) as CoordinatorLayout

        val glide: RequestManager = Glide.with(this)

        glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F87485472_103366637939081_20392040864940032_n.jpg?alt=media&token=ab6b18d4-1bfe-4728-aa0b-f9802689faf0")
            .into(advertisement1)

        glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F91292570_120727556202989_8307745143184687104_o.jpg?alt=media&token=342bcf09-fe28-4e1a-8d4d-16de79f98d39")
            .into(advertisement2)

        glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F95699352_132267178382360_1895322046422843392_o.jpg?alt=media&token=c7dd8313-3823-449f-b12e-fc564dea08e7")
            .into(advertisement3)

        glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F99142021_139159814359763_8267498293144059904_o.jpg?alt=media&token=0c06bbba-74bd-45cd-8d90-8bb39415ef82")
            .into(advertisement4)

    }

    private fun setListeners() {
        advertisement1.setOnClickListener {
            val glide: RequestManager = Glide.with(this)
            glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F87485472_103366637939081_20392040864940032_n.jpg?alt=media&token=ab6b18d4-1bfe-4728-aa0b-f9802689faf0")
                .into(fullImage)

            activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            fullImage.visibility = View.VISIBLE
            fullImage.scaleType = ImageView.ScaleType.FIT_CENTER
            coordinatorLayout.visibility = View.GONE

            advertisement1.visibility = View.INVISIBLE
            advertisement2.visibility = View.INVISIBLE
            advertisement3.visibility = View.INVISIBLE
            advertisement4.visibility = View.INVISIBLE

            fullImage.setOnClickListener {
                activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                fullImage.visibility = View.GONE

                advertisement1.visibility = View.VISIBLE
                advertisement2.visibility = View.VISIBLE
                advertisement3.visibility = View.VISIBLE
                advertisement4.visibility = View.VISIBLE

                coordinatorLayout.visibility = View.VISIBLE
            }
        }

        advertisement2.setOnClickListener {
            val glide: RequestManager = Glide.with(this)
            glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F91292570_120727556202989_8307745143184687104_o.jpg?alt=media&token=342bcf09-fe28-4e1a-8d4d-16de79f98d39")
                .into(fullImage)

            activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            fullImage.visibility = View.VISIBLE
            fullImage.scaleType = ImageView.ScaleType.FIT_CENTER
            coordinatorLayout.visibility = View.GONE

            advertisement1.visibility = View.INVISIBLE
            advertisement2.visibility = View.INVISIBLE
            advertisement3.visibility = View.INVISIBLE
            advertisement4.visibility = View.INVISIBLE

            fullImage.setOnClickListener {
                activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                fullImage.visibility = View.GONE

                advertisement1.visibility = View.VISIBLE
                advertisement2.visibility = View.VISIBLE
                advertisement3.visibility = View.VISIBLE
                advertisement4.visibility = View.VISIBLE

                coordinatorLayout.visibility = View.VISIBLE
            }
        }

        advertisement3.setOnClickListener {
            val glide: RequestManager = Glide.with(this)
            glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F95699352_132267178382360_1895322046422843392_o.jpg?alt=media&token=c7dd8313-3823-449f-b12e-fc564dea08e7")
                .into(fullImage)

            activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            fullImage.visibility = View.VISIBLE
            fullImage.scaleType = ImageView.ScaleType.FIT_CENTER
            coordinatorLayout.visibility = View.GONE

            advertisement1.visibility = View.INVISIBLE
            advertisement2.visibility = View.INVISIBLE
            advertisement3.visibility = View.INVISIBLE
            advertisement4.visibility = View.INVISIBLE

            fullImage.setOnClickListener {
                activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                fullImage.visibility = View.GONE

                advertisement1.visibility = View.VISIBLE
                advertisement2.visibility = View.VISIBLE
                advertisement3.visibility = View.VISIBLE
                advertisement4.visibility = View.VISIBLE

                coordinatorLayout.visibility = View.VISIBLE
            }
        }

        advertisement4.setOnClickListener {
            val glide: RequestManager = Glide.with(this)
            glide.load("https://firebasestorage.googleapis.com/v0/b/farmacias-gvs.appspot.com/o/advertisements%2F99142021_139159814359763_8267498293144059904_o.jpg?alt=media&token=0c06bbba-74bd-45cd-8d90-8bb39415ef82")
                .into(fullImage)

            activity!!.window.addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
            fullImage.visibility = View.VISIBLE
            fullImage.scaleType = ImageView.ScaleType.FIT_CENTER
            coordinatorLayout.visibility = View.GONE

            advertisement1.visibility = View.INVISIBLE
            advertisement2.visibility = View.INVISIBLE
            advertisement3.visibility = View.INVISIBLE
            advertisement4.visibility = View.INVISIBLE

            fullImage.setOnClickListener {
                activity!!.window.clearFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN)
                fullImage.visibility = View.GONE

                advertisement1.visibility = View.VISIBLE
                advertisement2.visibility = View.VISIBLE
                advertisement3.visibility = View.VISIBLE
                advertisement4.visibility = View.VISIBLE

                coordinatorLayout.visibility = View.VISIBLE
            }
        }
    }

}
