package com.gvs.farmacias_gvs_android.fragments

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.activities.ItemActivity
import com.gvs.farmacias_gvs_android.models.Item
import com.yqritc.recyclerviewflexibledivider.HorizontalDividerItemDecoration

class SearchFragment : Fragment() {

    companion object {
        fun newInstance(): SearchFragment = SearchFragment()
        val EXTRA_ITEM_ID = "XTRA_ITEMID"
    }

    private var searchText: EditText? = null
    private var searchButton: ImageButton? = null
    private lateinit var itemDatabase: DatabaseReference
    private var itemSearchList: RecyclerView? = null
    private lateinit var horizontalDivider: HorizontalDividerItemDecoration

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_search,container,false)
        horizontalDivider = HorizontalDividerItemDecoration.Builder(activity).size(1).margin(
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0f, resources.displayMetrics).toInt(), 0).build()

        findViews(view)
        setListeners()
        itemDatabase = FirebaseDatabase.getInstance().getReference("items")

        return view
    }

    private fun findViews(v: View) {
        searchText = v.findViewById<View>(R.id.searchText) as EditText
        searchButton = v.findViewById<View>(R.id.searchButton) as ImageButton
        itemSearchList = v.findViewById<View>(R.id.search_recycler_view) as RecyclerView
        itemSearchList!!.setHasFixedSize(true)
        itemSearchList!!.layoutManager = LinearLayoutManager(context)
        itemSearchList?.addItemDecoration(horizontalDivider)
    }

    private fun hideSoftKeyboard(activity: Activity){
        val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(searchButton!!.windowToken,0)
    }

    private fun setListeners() {
        searchText!!.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                val itemName = searchText!!.text.toString()
                if (!itemName.isBlank()){
                    val firstChar = itemName.subSequence(0,1) as String
                    val nextChar = itemName.subSequence(1,itemName.length)
                    val firstLetter = firstChar.toUpperCase()
                    val searchResult = firstLetter + nextChar
                    firebaseItemSearch(searchResult)
                } else{
                    Snackbar.make(searchButton!!, getString(R.string.search_missing_name), Snackbar.LENGTH_LONG).show()
                }
            }
        })
        searchButton!!.setOnClickListener {hideSoftKeyboard(this@SearchFragment.activity!!)
        }
    }

    private fun firebaseItemSearch(searchText: String) {
        val firebaseSearchQuery = itemDatabase
            .orderByChild("name")
            .startAt(searchText)
            .endAt(searchText + "\uf8ff")

        val firebaseRecyclerAdapter = object: FirebaseRecyclerAdapter<Item, ItemsViewHolder> (
            Item::class.java,
            R.layout.layout_search_result,
            ItemsViewHolder::class.java,
            firebaseSearchQuery
        )

        {
            override fun populateViewHolder(viewHolder: ItemsViewHolder, model: Item, position: Int) {
                viewHolder.setDetails(context!!,model.name,item = Pair(Item(), Item()))

                viewHolder.mView.setOnClickListener {
                    val itemID = model.itemId
                    val intent = Intent(context, ItemActivity::class.java)
                    intent.putExtra(SearchFragment.EXTRA_ITEM_ID,itemID)
                    context!!.startActivity(intent)
                }
            }
        }

        itemSearchList!!.adapter = firebaseRecyclerAdapter
    }

    class ItemsViewHolder(internal var mView: View) :RecyclerView.ViewHolder(mView) {
        fun setDetails(ctx: Context, itemName: String, item: Pair<Item, com.gvs.farmacias_gvs_android.models.Item>) {

            val itemNameResult = mView.findViewById<View>(R.id.search_name_item) as TextView

            itemNameResult.text = itemName
        }
    }

}
