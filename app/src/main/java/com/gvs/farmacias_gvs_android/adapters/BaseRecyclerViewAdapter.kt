package com.gvs.farmacias_gvs_android.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerViewAdapter<T> constructor(
    val context: Context,
    private var mListener: OnViewHolderClick<T>? = null
) : RecyclerView.Adapter<BaseRecyclerViewAdapter<T>.ViewHolder>() {

    var list: List<T>? = emptyList()

    interface OnViewHolderClick<T> {
        fun onClick(view: View, position: Int, item: T?)
    }

    inner class ViewHolder(view: View, private val mListener: OnViewHolderClick<T>?) : RecyclerView.ViewHolder(view),
        View.OnClickListener {
        private val mMapView: MutableMap<Int, View>

        init {
            mMapView = HashMap()
            mMapView.put(0, view)

            if (mListener != null)
                view.setOnClickListener(this)
        }

        override fun onClick(view: View) {
            mListener?.onClick(view, adapterPosition, getItem(adapterPosition))
        }

        fun getView(id: Int): View {
            if (mMapView.containsKey(id))
                return mMapView[id]!!
            else
                initViewById(id)
            return mMapView[id]!!
        }

        private fun getView(): View = getView(0)

        private fun initViewById(id: Int) {
            val view = getView().findViewById<View>(id)
            if (view != null) {
                mMapView.put(id, view)
            }
        }
    }

    protected abstract fun createView(context: Context, viewGroup: ViewGroup, viewType: Int): View

    protected abstract fun bindView(item: T?, position: Int, viewHolder: ViewHolder)

    //abstract fun contains(item: T): Boolean

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder =
        ViewHolder(createView(context, viewGroup, viewType), mListener)

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        if (getItem(position) != null)
            bindView(getItem(position)!!, position, viewHolder)
    }

    override fun getItemCount(): Int {
        if (list != null)
            return list!!.size
        return 0
    }

    private fun getItem(index: Int): T? {
        return if (list != null && index < list!!.size)
            list!![index]
        else
            null
    }

    fun setClickListener(listener: OnViewHolderClick<T>) {
        mListener = listener
    }
}