package com.gvs.farmacias_gvs_android.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gvs.farmacias_gvs_android.App
import com.gvs.farmacias_gvs_android.R
import com.gvs.farmacias_gvs_android.models.Item

class CartAdapter(context: Context, listener: BaseRecyclerViewAdapter.OnViewHolderClick<Item>) : BaseRecyclerViewAdapter<Item>(context, listener) {

    //private val glide: RequestManager = Glide.with(context)

    override fun createView(context: Context, viewGroup: ViewGroup, viewType: Int): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        return inflater.inflate(R.layout.layout_item, viewGroup, false)
    }

    override fun bindView(item: Item?, position: Int, viewHolder: ViewHolder) {
        if (item != null) {
            val itemImage = viewHolder.getView(R.id.layout_itemImage) as ImageView
            val itemName = viewHolder.getView(R.id.layout_itemName) as TextView
            val itemPrice = viewHolder.getView(R.id.layout_itemPrice) as TextView
            //val setQuantity = viewHolder.getView(R.id.setQuantity) as ImageView
            //val addToCart = viewHolder.getView(R.id.addToCart) as ImageView
            //val layoutItemQty = viewHolder.getView(R.id.layoutItemQty) as LinearLayout
            //val itemQty = viewHolder.getView(R.id.itemQty) as TextView
            //val itemQtyMinus = viewHolder.getView(R.id.itemQtyMinus) as TextView
            //val itemQtyPlus = viewHolder.getView(R.id.itemQtyPlus) as TextView

            //setQuantity.visibility = View.GONE
            //addToCart.visibility = View.GONE
            //layoutItemQty.visibility = addToCart.visibility

            itemName.text = item.name
            itemPrice.text = item.price.toString()

            /*glide.load(if (item.images.isEmpty())
                App.GVS_DEFAULT_MEDICINE_IMAGE_URL else item.images.firstOrNull { it.isNotBlank() || it.isNotEmpty() })
                .into(
                    itemImage
                )*/


        }
    }

}