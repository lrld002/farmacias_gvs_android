package com.gvs.farmacias_gvs_android

import android.app.Application
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.firestore.FirebaseFirestore
import com.gvs.farmacias_gvs_android.models.ShoppingCart
import com.gvs.farmacias_gvs_android.models.User

class App : Application() {

    override fun onCreate() {
        super.onCreate()

        FirebaseApp.initializeApp(this)
        val firebaseUser = FirebaseAuth.getInstance().currentUser
        if (firebaseUser != null) {
            FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_USERS)
                .document(firebaseUser.uid)
                .get()
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        user = it.result?.toObject(User::class.java) ?: User()
                        FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_SHOPPING_CARTS)
                            .whereEqualTo("id", user.uid)
                            .limit(1)
                            .get()
                            .addOnCompleteListener {task ->
                                if (task.isSuccessful){
                                    if (task.result != null){
                                        if (!task.result!!.isEmpty){
                                            shoppingCart = task.result!!.first().toObject(
                                                ShoppingCart::class.java)
                                        }else{
                                            shoppingCart = ShoppingCart(user.uid , user, ArrayList())
                                        }
                                    }else{
                                        shoppingCart = ShoppingCart(user.uid , user, ArrayList())
                                    }
                                }

                            }
                    }
                }

        }

        currentDatabase = FirebaseDatabase.getInstance()

        //database_table_users = currentDatabase.getReference(FIREBASE_DATABASE_TABLE_USERS)

        database_table_items = currentDatabase.getReference(FIREBASE_DATABASE_TABLE_ITEMS)

        //database_table_users.keepSynced(true)
        database_table_items.keepSynced(true)

    }

    companion object {

        fun getCart(user: User): ShoppingCart {
            return  FirebaseFirestore.getInstance().collection(FIRESTORE_COLLECTION_SHOPPING_CARTS)
                .whereEqualTo("id", user.uid)
                .limit(1)
                    .get()
                    .result?.first()?.toObject(ShoppingCart::class.java) ?: ShoppingCart(user.uid, user, ArrayList())
        }

        var user: User = User()
        var shoppingCart: ShoppingCart = ShoppingCart()

        lateinit var currentDatabase: FirebaseDatabase

        lateinit var database_table_users: DatabaseReference
        lateinit var database_table_items: DatabaseReference

        //val FIREBASE_DATABASE_TABLE_USERS = "users"
        val FIRESTORE_COLLECTION_USERS = "users"
        val FIRESTORE_COLLECTION_SHOPPING_CARTS = "Carts"

        val FIREBASE_DATABASE_TABLE_ITEMS = "items"

    }

}